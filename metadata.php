<?php

/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sMetadataVersion   = '2.0';
$aModule            = [
    'id'            => 'ecs_antinewsletter',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>AntiNewsletter</i>',
    'description'   => 'Newsletteranmeldung und Gaestebuch-Verlinkung entfernen.',
    'version'       => '2.0.6',
    'thumbnail'     => 'ecs.png',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend' => [
        \OxidEsales\Eshop\Application\Controller\NewsletterController::class => \Ecs\AntiNewsletter\Controller\NewsletterController::class,
    ],
    'settings' => [
        ['group' => 'ecs_main', 'name' => 'ecs_antinewsletter_focol', 'type' => 'bool', 'value' => true],
        ['group' => 'ecs_main', 'name' => 'ecs_antinewsletter_css',   'type' => 'bool', 'value' => false],
    ],
    'blocks' => [
        ['template' => 'layout/base.tpl',                    'block' => 'base_js',                   'file' => '/views/blocks/antinewsletter_base_js_ecs.tpl',  'position' => '-999'],
        ['template' => 'form/fieldset/order_newsletter.tpl', 'block' => 'user_billing_newsletter',   'file' => '/views/blocks/antinewsletter_ecs.tpl',          'position' => '-999'],
        ['template' => 'form/fieldset/user_account.tpl',     'block' => 'user_account_newsletter',   'file' => '/views/blocks/antinewsletter_ecs.tpl',          'position' => '-999'],
        ['template' => 'form/fieldset/user_noaccount.tpl',   'block' => 'user_noaccount_newsletter', 'file' => '/views/blocks/antinewsletter_ecs.tpl',          'position' => '-999'],
        ['template' => 'layout/footer.tpl',                  'block' => 'dd_footer_newsletter',      'file' => '/views/blocks/antinewsletter2_ecs.tpl',         'position' => '-999'],
    ],
    'events' => [
        'onActivate'   => 'Ecs\AntiNewsletter\Core\Events::onActivate',
        'onDeactivate' => 'Ecs\AntiNewsletter\Core\Events::onDeactivate',
    ],
];
