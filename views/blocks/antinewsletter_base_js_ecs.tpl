[{strip}]
[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]
[{assign var="oConf" value=$oViewConf->getConfig()}]
[{if $oConf->getConfigParam('ecs_antinewsletter_css') }]
    <style type="text/css">.hidden{display:none!important;}</style>
[{/if}]
[{capture assign=nonews}]
    $('a[href*="newsletter"],.newsletter').addClass("hidden");
    $('#linkAccountNewsletter').unwrap();
    $('a[href*="newsletter"]').parent().remove();
    $('a[href*="newsletter"]').remove();
    $('.newsletter').remove();
    [{if $oConf->getConfigParam('ecs_antinewsletter_focol') }]
        $('.footer-box-newsletter').addClass("hidden");
        $('#footer .row div').removeClass("col-md-8");
        $('#footer .row div').removeClass("col-lg-8");
    [{/if}]
[{/capture}]
[{oxscript add=$nonews}]
[{$smarty.block.parent}]
[{/strip}]